//
//  UrlEnums.swift
//  messageDashBoard
//
//  Created by Moses Olawoyin on 03/11/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import Foundation

enum EndPoints {
    
    static let users = "/users"
    static let posts = "/posts"
    static let comments = "/comments"
    static let avatars = "/avatars"
}

enum Typpicode {
    
    static let scheme = "http"
    static let host = "jsonplaceholder.typicode.com"
}

enum Avartars {
    
    static let scheme = "https"
    static let host = "avatars.adorable.io"
}
