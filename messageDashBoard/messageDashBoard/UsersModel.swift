//
//  UsersModel.swift
//  messageDashBoard
//
//  Created by Moses Olawoyin on 03/11/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import Foundation


//struct Root: Codable{
//    var user:[User]
//}

struct User: Codable{
    var id: Int
    var name: String
    var username: String
    var email: String
    var address: [AddressInfo]
    var phone: String
    var website: String
    
}

struct AddressInfo: Codable{
    var street: String
    var suite: String
    var city: String
    var zipcode: String
}

class UserModel{
    var users:[User]
    
    init() {
        users = []
    }
}
